from django.urls import path
from . import views
app_name = 'components.core'
urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('product/<pk>/', views.ProductView.as_view(), name='product'),
    path('add-to-cart/<pk>/', views.add_to_cart, name='add-to-cart'),
    path('remove-from-cart/<pk>/', views.remove_from_cart, name='remove-from-cart'),
    path('order-summary', views.OrderSummaryView.as_view(), name='order-summary'),
    path('reduce-quantity-item/<pk>/',
         views.reduce_quantity_item, name='reduce-quantity-item')
]
